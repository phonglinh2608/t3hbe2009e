﻿using DAL.DataModel.BaseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.DataModel.BusinessModel
{
    [Serializable]
    [Table("Category")]
    public class Category:BaseEntity
    {
        public string CategoryName { get; set; }        
    }
}
