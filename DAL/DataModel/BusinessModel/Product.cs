﻿using DAL.DataModel.BaseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.DataModel.BusinessModel
{
    [Serializable]
    [Table("Product")]
    public class Product: BaseEntity
    {
        public string ProductName { get; set; }
        public string Color { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Description { get; set; }
        public int Price { get; set; }
        public double Discount { get; set; }
        public int CategoryId { get; set; }        
    }
}
