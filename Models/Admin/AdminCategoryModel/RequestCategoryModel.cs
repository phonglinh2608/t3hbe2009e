﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Admin.AdminCategoryModel
{
    public class RequestAddNewCategoryModel
    {
        public string category_name { get; set; }
        public int creators { get; set; }
    }
}
