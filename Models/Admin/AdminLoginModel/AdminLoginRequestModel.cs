﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Admin.AdminLoginModel
{
    public class AdminLoginRequestModel
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
