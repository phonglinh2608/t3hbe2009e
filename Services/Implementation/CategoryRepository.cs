﻿using DAL.DataModel.BusinessModel;
using DAL.DBContext;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Implementation
{
    public class CategoryRepository : IRepository<Category>
    {
        private ShopContext db;
        public CategoryRepository(ShopContext shopContext)
        {
            db = shopContext;
        }
        public void Add(Category entity)
        {
            db.Categories.Add(entity);
            db.SaveChanges();
        }

        public int Delete(int id, int userid)
        {
            var selectedCategory = db.Categories.Where(c => c.IsDeleted == false && c.Id == id).FirstOrDefault();
            if (selectedCategory == null)
            {
                return 0;
            }
            selectedCategory.IsDeleted = true;
            selectedCategory.DeletedDate = DateTime.Now;
            selectedCategory.DeletedBy = userid;
            db.SaveChanges();
            return 1;
        }

        public void Edit(int originalId,Category entity,int userid)
        {
            var updateCategory = db.Categories.Where(x => x.Id == originalId && x.IsDeleted == false).FirstOrDefault();
            if (updateCategory == null)
            { 
                // return BadRequest(); 
            }
            updateCategory.CategoryName = entity.CategoryName;
            updateCategory.ModifiedBy = userid;
            updateCategory.ModifiedDate = DateTime.Now;
            db.SaveChanges();
        }

        public Category GetById(int id)
        {
            return db.Categories.Where(c => c.Id == id && c.IsDeleted == false).FirstOrDefault();
        }

        public IEnumerable<Category> List()
        {
            return db.Categories.Where(c => c.IsDeleted == false).ToList();
        }

        public IEnumerable<Category> List(Expression<Func<Category, bool>> predicate)
        {
            return db.Categories.Where(predicate).Where(c => c.IsDeleted == false).ToList();
        }
    }
}
