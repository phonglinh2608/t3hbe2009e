﻿using DAL.DBContext;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Implementation
{
    public class CommitChange:ICommitChange
    {
        private ShopContext db;
        public CommitChange(ShopContext shopContext)
        {
            db = shopContext;
        }

        public void SaveChange()
        {
            db.SaveChanges();
        }
    }
}
