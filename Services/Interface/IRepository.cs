﻿using DAL.DataModel.BaseModel;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Services.Interface
{
    public interface IRepository<T>
    {
        T GetById(int id);
        IEnumerable<T> List();
        IEnumerable<T> List(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        int Delete(int id, int userid);
        void Edit(int originalId, T entity, int userid);        
    }
}
