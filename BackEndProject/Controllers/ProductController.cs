﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.DBContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Models.Admin.AdminCategoryModel;

namespace BackEndProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private ShopContext _shopContext;
        public ProductController(ShopContext shopContext)
        {
            _shopContext = shopContext;
        }
        [HttpGet]
        [Route("category/{id}")]
        public async Task<ResponeMessageForList<string>> GetProducts(int id)
        {
            var list = _shopContext.Categories.Where(c=>c.Id==id && c.IsDeleted==false).ToList();
            var result = new List<string>();
            foreach(var item in list)
            {
                result.Add(item.CategoryName);
            }
            return await Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công",
                Data = result
            });          
        }
        [HttpPost]
        public Task<ResponeMessageForList<string>> AddNewCategory([FromBody] RequestAddNewCategoryModel categoryModel)
        {
            _shopContext.Categories.Add(new DAL.DataModel.BusinessModel.Category
            {
                CategoryName = categoryModel.category_name,
                IsDeleted = false,
                CreatedDate=DateTime.Now,
                CreatedBy=1
            });
            _shopContext.SaveChanges();
            return Task.FromResult(new ResponeMessageForList<string>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công"                
            });
        }
    }
}
