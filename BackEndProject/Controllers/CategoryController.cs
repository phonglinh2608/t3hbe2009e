﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.DataModel.BusinessModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interface;

namespace BackEndProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        // this is for category CRUD
        private IRepository<Category> _repository;
        public CategoryController(IRepository<Category> repository)
        {
            _repository = repository;
        }
        [HttpGet]
        [Route("category/{id}")]
        public async Task<ResponseMessageForList<Category>> GetCategory(int id)
        {
            var result = _repository.GetById(id);
            
            return await Task.FromResult(new ResponseMessageForList<Category>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công",
                Data = result
            });
        }
        [HttpGet]
        [Route("category")]
        public async Task<ResponeMessageForList<Category>> GetCategoryList()
        {
            var result = _repository.List();

            return await Task.FromResult(new ResponeMessageForList<Category>
            {
                ErrorCode = "00",
                ErrorMessage = "Thành công",
                Data = result.ToList()
            });
        }
    }
}
